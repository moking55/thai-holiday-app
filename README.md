# Repository Overview

This repository contains a project developed using Kotlin, Java, and Gradle. The project is an Android application that utilizes SQLite for data storage and retrieval. The application displays a list of holidays, including their names and dates.

## Technologies Used

- **Kotlin**: The primary language used for development.
- **Java**: Used in some parts of the project for specific functionalities.
- **Gradle**: Used for dependency management and build automation.

## Features

- **SQLite Database**: The application uses SQLite for storing and retrieving holiday data.
- **ArrayAdapter**: The application uses an ArrayAdapter to display the list of holidays in a ListView. The ArrayAdapter is customized to handle a layout with two TextViews, allowing both the holiday name and date to be displayed in each list item.

## How to Run

To run the application, you need to have Android Studio installed. Open the project in Android Studio, build it, and run it on an emulator or a connected Android device.

## Code Structure

The code is structured following best practices for Android development. It includes the use of Cursors for database operations and custom Adapters for displaying complex data in ListViews.
