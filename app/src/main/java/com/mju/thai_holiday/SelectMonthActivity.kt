package com.mju.thai_holiday

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ListView

class SelectMonthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_month)

        val exitAppButton : Button = findViewById(R.id.btn_exit_app)
        exitAppButton.setOnClickListener {
            finishAffinity()
        }

        val monthSelection: ListView = findViewById(R.id.list_month)
        val monthList: ArrayList<String> = arrayListOf(
            "มกราคม",
            "กุมภาพันธ์",
            "มีนาคม",
            "เมษายน",
            "พฤษภาคม",
            "มิถุนายน",
            "กรกฎาคม",
            "สิงหาคม",
            "กันยายน",
            "ตุลาคม",
            "พฤศจิกายน",
            "ธันวาคม"
        )

        monthSelection.setOnItemClickListener { parent, view, position, id ->
            val selectedItem = parent.getItemAtPosition(position).toString()
            Log.d("Selected Month", selectedItem)
            val intent = Intent(this, SelectedMonthActivity::class.java)
            intent.putExtra("selectedMonth", selectedItem)
            startActivity(intent)
        }

        val adapter = MonthAdapter(this, android.R.layout.simple_list_item_1, monthList)
        monthSelection.adapter = adapter
    }
}