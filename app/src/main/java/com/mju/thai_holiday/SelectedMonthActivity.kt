package com.mju.thai_holiday

import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ListView
import android.widget.TextView

class SelectedMonthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selected_month)

        val selectedMonth = intent.getStringExtra("selectedMonth")
        val selectedMonthTextView: TextView = findViewById(R.id.label_month_name)
        selectedMonthTextView.text = selectedMonth

        val db: SQLiteDatabase = this.openOrCreateDatabase("thai_holiday", MODE_PRIVATE, null)
        val cursor = db.rawQuery("SELECT * FROM holiday WHERE month = '$selectedMonth'", null)
        val holidayList: ArrayList<String> = ArrayList()
        val holidayDateList: ArrayList<String> = ArrayList()
//        Log data from cursor
        if (cursor.moveToFirst()) {
            do {
                val holidayNameIndex = cursor.getColumnIndexOrThrow("name")
                val holidayDateIndex = cursor.getColumnIndexOrThrow("date")
                val holidayMonthIndex = cursor.getColumnIndexOrThrow("month")
                holidayList.add(cursor.getString(holidayNameIndex))
                holidayDateList.add(cursor.getString(holidayDateIndex) + " " + cursor.getString(holidayMonthIndex))
            } while (cursor.moveToNext())
        }
        cursor.close()

//        combine holidayList and holidayDateList into a map
        val holidayMap = holidayList.zip(holidayDateList).toMap()
        Log.d("holidayMap", holidayMap.toString())
        val holidayListView: ListView = findViewById(R.id.holiday_list)
        val adapter = HolidayAdapter(this, holidayMap)
        holidayListView.adapter = adapter

        val backBtn: Button = findViewById(R.id.go_back_btn)
        backBtn.setOnClickListener {
            finish()
        }
    }
}