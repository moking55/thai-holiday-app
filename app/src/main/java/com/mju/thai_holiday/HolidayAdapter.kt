package com.mju.thai_holiday

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView

class HolidayAdapter(context: Context, private val holidayMap: Map<String, String>) : BaseAdapter() {
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return holidayMap.size
    }

    override fun getItem(position: Int): Map.Entry<String, String> {
        return holidayMap.entries.toList()[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val vh: ItemHolder

        if (convertView == null) {
            view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false)
            vh = ItemHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }

        vh.label.text = getItem(position).key
        vh.data.text = getItem(position).value

        return view
    }

    private class ItemHolder(row: View?) {
        val label: TextView = row?.findViewById(android.R.id.text1) as TextView
        val data: TextView = row?.findViewById(android.R.id.text2) as TextView
    }
}
