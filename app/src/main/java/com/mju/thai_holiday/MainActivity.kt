package com.mju.thai_holiday

import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!doesDatabaseExist(this, "thai_holiday")) {
            Log.w("MainActivity", "Database does not exist, creating database")
            val thai_holiday = mapOf<String, List<Map<String, String>>>(
                "มกราคม" to listOf(
                    mapOf("name" to "วันขึ้นปีใหม่", "date" to "1")
                ),
                "กุมภาพันธ์" to listOf(
                    mapOf("name" to "วันมาฆบูชา", "date" to "24"),
                    mapOf("name" to "หยุดชดเชยวันมาฆบูชา", "date" to "26")
                ),
                "เมษายน" to listOf(
                    mapOf("name" to "วันจักรี", "date" to "6"),
                    mapOf("name" to "วันหยุดชดเชยวันจักรี", "date" to "8"),
                    mapOf("name" to "วันหยุดกรณีพิเศษ ตามมติ ครม", "date" to "12"),
                    mapOf("name" to "วันสงกรานต์", "date" to "13"),
                    mapOf("name" to "วันสงกรานต์", "date" to "14"),
                    mapOf("name" to "วันสงกรานต์", "date" to "15"),
                    mapOf("name" to "วันหยุดชดเชยวันสงกรานต์", "date" to "16")
                ),
                "พฤษภาคม" to listOf(
                    mapOf("name" to "วันแรงงาน", "date" to "1"),
                    mapOf("name" to "วันฉัตรมงคล", "date" to "4"),
                    mapOf("name" to "วันหยุดชดเชยวันฉัตรมงคล", "date" to "6"),
                    mapOf("name" to "วันวิสาขบูชา", "date" to "22")
                ),
                "มิถุนายน" to listOf(
                    mapOf("name" to "วันเฉลิมพระชนมพรรษาสมเด็จพระราชินี", "date" to "3")
                ),
                "กรกฎาคม" to listOf(
                    mapOf("name" to "วันอาสาฬหบูชา", "date" to "20"),
                    mapOf("name" to "วันเข้าพรรษา", "date" to "21"),
                    mapOf("name" to "วันหยุดชดเชยวันเข้าพรรษา", "date" to "22"),
                    mapOf(
                        "name" to "วันเฉลิมพระชนมพรรษาพระบาทสมเด็จพระวชิรเกล้าเจ้าอยู่หัว รัชกาลที่ 10",
                        "date" to "28"
                    ),
                    mapOf(
                        "name" to "วันหยุดชดเชยวันเฉลิมพระชนมพรรษาพระบาทสมเด็จพระวชิรเกล้าเจ้าอยู่หัว รัชกาลที่ 10",
                        "date" to "29"
                    ),
                ),
                "สิงหาคม" to listOf(
                    mapOf(
                        "name" to "วันเฉลิมพระชนมพรรษาสมเด็จพระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ พระบรมราชชนนีพันปีหลวง",
                        "date" to "12"
                    ),
                ),
                "ตุลาคม" to listOf(
                    mapOf(
                        "name" to "วันคล้ายวันสวรรคตพระบาทสมเด็จพระบรมชนกาธิเบศร มหาภูมิพลอดุลยเดชมหาราช บรมนาถบพิตร",
                        "date" to "13"
                    ),
                    mapOf(
                        "name" to "วันหยุดชดเชยคล้ายวันสวรรคตพระบาทสมเด็จพระบรมชนกาธิเบศรฯ",
                        "date" to "14"
                    ),
                    mapOf("name" to "วันปิยมหาราช", "date" to "23"),
                ),
                "ธันวาคม" to listOf(
                    mapOf("name" to "วันพ่อแห่งชาติ", "date" to "5"),
                    mapOf("name" to "วันรัฐธรรมนูญ", "date" to "10"),
                    mapOf("name" to "วันสิ้นปี", "date" to "31"),
                )
            )


            val db: SQLiteDatabase = openOrCreateDatabase("thai_holiday", MODE_PRIVATE, null)
            db.execSQL("CREATE TABLE IF NOT EXISTS holiday (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, date TEXT, month TEXT)")
            thai_holiday.forEach { (month, holidays) ->
                holidays.forEach { holiday ->
                    val name = holiday["name"]
                    val date = holiday["date"]
                    db.execSQL("INSERT INTO holiday (name, date, month) VALUES ('$name', '$date', '$month')")
                    Log.d("MainActivity", "Inserted $name on $date $month to database")
                }
            }
            db.close()
        } else {
            Log.d("MainActivity", "Database already exists continuing to main activity")
        }

        val exitAppBtn: Button = findViewById(R.id.btn_exit_app)
        exitAppBtn.setOnClickListener {
            finishAffinity()
        }

        val enterApp: Button = findViewById(R.id.btn_enter_app)
        enterApp.setOnClickListener {
            val intent = Intent(this, SelectMonthActivity::class.java)
            startActivity(intent)
        }
    }

    private fun doesDatabaseExist(context: Context, dbName: String): Boolean {
        val dbFile = context.getDatabasePath(dbName)
        return dbFile.exists()
    }
}